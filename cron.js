'use strict'
const CronJob = require('cron').CronJob
const db = require('./db')
const Mandrill = require('./providers/mandrill')
let isSending = false
new CronJob('*/5 * * * * *', async () => {
  console.log(`run ${new Date().toLocaleString()}`)
  if (!isSending) {
    const services = await db()
    const MandrillObj = new Mandrill(services)
    isSending = true
    await MandrillObj.mailsSender().catch(handleFatalError)
    isSending = false
  }
}, null, true, 'America/Guayaquil')

// inspectorMails()
//
// sendEmailSession()

async function sendEmailSession () {
  const services = await db()
  const MandrillObj = new Mandrill(services)
  await MandrillObj.mailsSender().catch(handleFatalError)
  process.exit(0)
}

function inspectorMails () {
  return new Promise(async (resolve, reject) => {
    const services = await db()
    const MandrillObj = new Mandrill(services)
    await MandrillObj.inspectorMails()
    resolve(true)
  })
}

function handleFatalError (err) {
  console.error(err.message)
  console.error(err.stack)
  process.exit(1)
}
