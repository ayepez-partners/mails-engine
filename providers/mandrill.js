'use strict'
const mandrill = require('mandrill-api/mandrill')
const { mandrillKey } = require('../config')
const mandrillClient = new mandrill.Mandrill(mandrillKey)
const { map, find } = require('lodash')
const moment = require('moment')

class Mandrill {
  constructor (service) {
    this.SendBulk = service.SendBulk
  }

  mailsSender () {
    return new Promise(async (resolve, reject) => {
      let sessions = await this.SendBulk.finAllSessions(
        { where: { estado: 'PENDIENTE' } }).catch(Mandrill.handleFatalError)
      try {
        for (let session of sessions) {
          session.mailConfig = JSON.parse(session.jsonConfigMail)
          session.filters = JSON.parse(session.jsonFilter)
          await this.SendBulk.updateSession(
            { id: session.id, estado: 'EN_PROGRESO' }).catch(Mandrill.handleFatalError)
          let pageSize = 1000
          let clients = await this.getClients({ session, pageSize }).catch(Mandrill.handleFatalError)
          while (clients.count && clients.count > 0) {
            const resultSave = await this.SendBulk.bulkCreateOrUpdate(clients.rows.model).catch(Mandrill.handleFatalError)
            if (resultSave) {
              const resultSend = await this.sendMail(clients.rows.mail, resultSave).catch(Mandrill.handleFatalError)
              await this.SendBulk.bulkCreateOrUpdate(resultSend).catch(Mandrill.handleFatalError)
            }
            clients = await this.getClients({ session, pageSize }).catch(Mandrill.handleFatalError)
          }
        }
        resolve(true)
      } catch (e) {
        reject(e)
      }
    })
  }

  getClients ({ session, page, pageSize }) {
    const { mailConfig, filters } = session
    console.log(mailConfig)
    const mailSendConfig = {
      ...mailConfig.mail
    }
    const mergeVars = []
    const toConfig = []
    return new Promise(async (resolve, reject) => {
      try {
        this.SendBulk.findAllSessionClients({
          where: {
            campaniaId: filters.campania_id,
            mailSessionId: session.id,
          },
          page,
          pageSize,
        }).then((result) => {
          const clientModels = []
          for (let client of result.rows) {
            const details = client.details ? JSON.parse(
              client.details) : null
            toConfig.push({
              email: client.email,
              name: client.fullName,
              type: 'to'
            })
            if (details) {
              mergeVars.push({
                rcpt: client.email,
                vars: map(details, (value, key) => ({ name: key, content: value }))
              })
            }
            clientModels.push({
              from: mailConfig.mail.from_email,
              asunto: mailConfig.mail.subject,
              contenido: mailConfig.mail.html,
              email: client.email,
              fechaCreacion: new Date().toLocaleString(),
              estado: 'PENDIENTE',
              contactoId: client.clientId,
              campaniaId: session.campaniaId,
              medioEnvio: 'MANDRILL',
              mailSessionId: session.id,
              usuarioCreacionId: session.usuarioCreacionId
            })
          }
          result.rows = { model: clientModels, mail: { ...mailSendConfig, to: toConfig, merge_vars: mergeVars } }
          resolve(result)
        }).catch(reject)
      } catch (e) {
        reject(e)
      }
    })
  }

  sendMail (message, models) {
    return new Promise((resolve, reject) => {
      const success = result => {
        const results = []
        if (result) {
          map(result, (row) => {
            const model = find(models, model => (model.email === row.email))
            let statusSend = 'ENVIADO'
            switch (row.status) {
              case 'queued':
                statusSend = 'EN_COLA'
                break
              case 'rejected' || 'invalid' || 'scheduled':
                statusSend = 'NO_ENVIADO'
                break
            }
            results.push({
              id: model.id, estado: statusSend,
              fechaEnvio: new Date().toLocaleString(),
              mandrillResponse: JSON.stringify(row),
              mandrillId: row._id,
            })
          })
        }
        resolve(results)
      }
      const err = err => {
        const results = []
        map(models, (row) => {
          results.push({
            id: row.id, estado: 'NO_ENVIADO',
            mandrillResponse: JSON.stringify(err)
          })
        })
        resolve(results)
      }
      try {
        if (message.template_name) {
          mandrillClient.messages.sendTemplate({
            template_name: message.template_name,
            template_content: [],
            message,
            async: false,
          }, success, err)
        } else {
          mandrillClient.messages.send(
            { message: message, async: false },
            success, err)
        }
      } catch (e) {
        reject(e)
      }

    })
  }

  inspectorMails () {
    let page = 1
    const limit = 1000
    return new Promise(async (resolve) => {
      let data = await this.getMailsToInspector(page, limit)
      while (data.rows && data.rows.length > 0) {
        for (let mail of data.rows) {
          await this.updateDataMail(mail)
        }
        page++
        data = await this.getMailsToInspector(page, limit)
      }
      resolve({})
    })
  }

  getMailsToInspector (page, limit) {
    return this.SendBulk.findAllMails({
      where: {
        fechaEnvio: {
          [Symbol.for('between')]: [
            moment().subtract(10, 'd').format('YYYY-MM-DD 00:00:00'),
            moment().format('YYYY-MM-DD 23:59:59'),
          ],
        },
        // mailSessionId: 16
        inspectorAt: { [Symbol.for('eq')]: null },
      },
      page,
      pageSize: limit,
    })
  }

  updateDataMail (mail) {
    let that = this
    return new Promise((resolve) => {
      mandrillClient.messages.info({ id: mail.mandrillId },
        async function (result) {
          const estado = result.state === 'sent' ? 'ENVIADO' : (
            ['bounced', 'rejected'].indexOf(result.state) >= 0
              ? 'NO_ENVIADO'
              : 'PENDIENTE'
          )
          const resultSave = await that.SendBulk.createOrUpdateMail({
            id: mail.id,
            opens: result.opens,
            opensDetails: JSON.stringify(result.opens_detail),
            clicks: result.clicks,
            clicksDetails: JSON.stringify(result.clicks_detail),
            inspectorResponse: JSON.stringify(result),
            inspectorAt: moment().format('YYYY-MM-DD HH:mm:ss'),
            estado,
          }).catch(console.error)
          resolve(resultSave)
        }, async function (e) {
          const resultSave = await that.SendBulk.createOrUpdateMail({
            id: mail.id,
            inspectorResponse: JSON.stringify(e),
            inspectorAt: moment().format('YYYY-MM-DD HH:mm:ss'),
            estado: 'NO_ENVIADO',
          })
          resolve(resultSave)
        })
    })
  }

  static handleFatalError (err) {
    console.error(err.message)
    console.error(err.stack)
    // process.exit(1)
  }
}

module.exports = Mandrill
