'use strict'

const { db } = require('../config')
const setupDatabase = require('./libs/db')
const setupCampaignClientOwnerModel = require('./models/campaign_client_owner')
const setupCampaignModel = require('./models/campaign')
const setupMailModel = require('./models/mail')
const setupMailSessionModel = require('./models/mail_session')
const setupMarketingNotClientModel = require('./models/marketing_no_clients')
const setupSendBulk = require('./libs/mail/send-bulk')

module.exports = async function () {
  const config = {
    ...db.main, logging: console.log,
    query: {
      raw: true
    }
  }

  const sequelize = setupDatabase(config)

  const CampaignModel = setupCampaignModel(config)
  const CampaignClientOwnerModel = setupCampaignClientOwnerModel(config)
  const MailModel = setupMailModel(config)
  const MailSessionModel = setupMailSessionModel(config)
  const MarketingNotClientModel = setupMarketingNotClientModel(config)

  CampaignClientOwnerModel.belongsTo(CampaignModel, { foreignKey: 'campania_id' })
  CampaignClientOwnerModel.belongsTo(MarketingNotClientModel, { foreignKey: 'cliente_id' })
  CampaignClientOwnerModel.belongsTo(MailModel, { foreignKey: 'cliente_id' })

  await sequelize.authenticate()

  const SendBulk = setupSendBulk(CampaignClientOwnerModel, MailModel, MailSessionModel,
    MarketingNotClientModel, CampaignModel)

  return {
    SendBulk
  }
}
