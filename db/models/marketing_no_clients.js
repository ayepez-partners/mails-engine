'use strict'

const DataTypes = require('sequelize').DataTypes
const setupDatabase = require('../libs/db')

module.exports = function setupMarketingNotClientModel (config) {
  const sequelize = setupDatabase(config)
  return sequelize.define('marketingNoClientes', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    documento: {
      type: DataTypes.STRING(45),
      allowNull: true,
      field: 'documento'
    },
    nombres: {
      type: DataTypes.STRING(100),
      allowNull: true,
      field: 'nombres'
    },
    apellidos: {
      type: DataTypes.STRING(100),
      allowNull: false,
      field: 'apellidos'
    },
    correo: {
      type: DataTypes.STRING(100),
      allowNull: false,
      unique: true,
      field: 'correo'
    },
    campaniaId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'campania_id'
    },
    details: {
      type: DataTypes.STRING,
      allowNull: true,
      field: 'merge_vars'
    }
  }, {
    tableName: 'marketing_no_clientes', timestamps: false
  })
}
