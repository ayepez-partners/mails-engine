'use strict'

const DataTypes = require('sequelize').DataTypes
const setupDatabase = require('../libs/db')

module.exports = function setupCampaignModel (config) {
  const sequelize = setupDatabase(config)
  return sequelize.define('campania', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    nombre: {
      type: DataTypes.STRING(64),
      allowNull: false,
      field: 'nombre'
    },
    fechaInicio: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'fecha_inicio'
    },
    fechaFin: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'fecha_fin'
    },
    formularios: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '0',
      field: 'formularios'
    },
    entidadTipo: {
      type: DataTypes.STRING(64),
      allowNull: true,
      field: 'entidad_tipo'
    },
    actualizacionInfo: {
      type: DataTypes.INTEGER(4),
      allowNull: true,
      defaultValue: '0',
      field: 'actualizacion_info'
    },
    estado: {
      type: DataTypes.ENUM('CREADA', 'INICIADA', 'EN PROGRESO', 'FINALIZADA', 'ANULADA'),
      allowNull: false,
      defaultValue: 'CREADA',
      field: 'estado'
    },
    usuarioCreacionId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      field: 'usuario_creacion_id'
    },
    usuarioActualizacionId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'usuario_actualizacion_id'
    },
    fechaCreacion: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'fecha_creacion'
    },
    fechaActualizacion: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'fecha_actualizacion'
    },
    noseyFormId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'nosey_form_id'
    },
    motivoLlamadaId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'motivo_llamada_id'
    },
    submotivoLlamadaId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'submotivo_llamada_id'
    },
    tipoCampaniaId: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      references: {
        model: 'tipo_campania',
        key: 'id'
      },
      field: 'tipo_campania_id'
    }
  }, {
    tableName: 'campania', timestamps: false
  })
}
