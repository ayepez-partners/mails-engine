'use strict'

const DataTypes = require('sequelize').DataTypes
const setupDatabase = require('../libs/db')

module.exports = function setupMailModel (config) {
  const sequelize = setupDatabase(config)
  return sequelize.define('mail', {
    id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'id'
    },
    from: {
      type: DataTypes.STRING(50),
      allowNull: true,
      field: 'from'
    },
    asunto: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'asunto'
    },
    contenido: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'contenido'
    },
    email: {
      type: DataTypes.STRING(45),
      allowNull: false,
      field: 'email'
    },
    fechaCreacion: {
      type: DataTypes.DATE,
      allowNull: false,
      field: 'fecha_creacion'
    },
    fechaEnvio: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'fecha_envio'
    },
    usuarioCreacionId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'usuario_creacion_id'
    },
    estado: {
      type: DataTypes.ENUM('PENDIENTE', 'ENVIADO', 'NO_ENVIADO'),
      allowNull: false,
      defaultValue: 'PENDIENTE',
      field: 'estado'
    },
    contactoId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      field: 'contacto_id'
    },
    asuntoId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'mail_asunto',
        key: 'id'
      },
      field: 'asunto_id'
    },
    plantillaId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      references: {
        model: 'mail_plantilla',
        key: 'id'
      },
      field: 'plantilla_id'
    },
    medioEnvio: {
      type: DataTypes.STRING(45),
      allowNull: false,
      field: 'medio_envio'
    },
    mandrillId: {
      type: DataTypes.STRING(200),
      allowNull: true,
      field: 'mandrill_id'
    },
    fechaActualizacion: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'fecha_actualizacion'
    },
    campaniaId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'campania_id'
    },
    mailSessionId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'mail_session_id'
    },
    mandrillResponse: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'mandrill_response'
    },
    opens: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'opens'
    },
    opensDetails: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'opens_details'
    },
    clicks: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'clicks'
    },
    clicksDetails: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'clicks_details'
    },
    inspectorResponse: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'inspector_response'
    },
    inspectorAt: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'inspector_at'
    }
  }, {
    tableName: 'mail', timestamps: false
  })
}
