'use strict'

const DataTypes = require('sequelize').DataTypes
const setupDatabase = require('../libs/db')

module.exports = function setupMailSessionModel (config) {
  const sequelize = setupDatabase(config)
  return sequelize.define('mailSession', {
    id: {
      type: DataTypes.INTEGER(11),
      allowNull: false,
      primaryKey: true,
      autoIncrement: true,
      field: 'Id'
    },
    asunto: {
      type: DataTypes.STRING(200),
      allowNull: false,
      defaultValue: '',
      field: 'asunto'
    },
    estado: {
      type: DataTypes.ENUM('PENDIENTE', 'EN_PROGRESO', 'FINALIZADO'),
      allowNull: false,
      defaultValue: 'PENDIENTE',
      field: 'estado'
    },
    jsonFilter: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'json_filter'
    },
    numTotal: {
      type: DataTypes.INTEGER(10),
      allowNull: false,
      defaultValue: '0',
      field: 'num_total'
    },
    numPendientes: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0',
      field: 'num_pendientes'
    },
    numCola: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0',
      field: 'num_cola'
    },
    numEnviados: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0',
      field: 'num_enviados'
    },
    numNoenviados: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      defaultValue: '0',
      field: 'num_noenviados'
    },
    tag: {
      type: DataTypes.STRING(45),
      allowNull: false,
      defaultValue: '',
      field: 'tag'
    },
    jsonConfigMail: {
      type: DataTypes.TEXT,
      allowNull: false,
      field: 'json_config_mail'
    },
    usuarioCreacionId: {
      type: DataTypes.INTEGER(11),
      allowNull: true,
      field: 'usuario_creacion_id'
    },
    fechaCreacion: {
      type: DataTypes.DATE,
      allowNull: false,
      defaultValue: '0000-00-00 00:00:00',
      field: 'fecha_creacion'
    },
    fechaFinalizacion: {
      type: DataTypes.DATE,
      allowNull: true,
      field: 'fecha_finalizacion'
    }
  }, {
    tableName: 'mail_session', timestamps: false
  })
}
