'use strict'

const DataTypes = require('sequelize').DataTypes
const setupDatabase = require('../libs/db')

module.exports = function setupCampaignClientOwnerModel (config) {
  const sequelize = setupDatabase(config)
  return sequelize.define('campaniaClientesOwner', {
    id: {
      type: DataTypes.INTEGER, allowNull: false, primaryKey: true, autoIncrement: true, field: 'id'
    },
    ownerId: { type: DataTypes.INTEGER, allowNull: false, field: 'owner_id' },
    campaniaId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      references: { model: 'campania', key: 'id' },
      field: 'campania_id'
    },
    clienteId: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: '0',
      field: 'cliente_id'
    },
    entidadId: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'entidad_id'
    },
    observaciones: {
      type: DataTypes.TEXT,
      allowNull: true,
      field: 'observaciones'
    },
    cantidadMail: {
      type: DataTypes.INTEGER,
      allowNull: true,
      field: 'cantidad_mail'
    }
  }, {
    tableName: 'campania_clientes_owner', timestamps: false
  })
}
