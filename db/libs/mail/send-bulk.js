'use strict'
const { QueryTypes } = require('sequelize')
module.exports = function setupSendBulk (
  CampaignClientOwnerModel, MailModel, MailSessionModel) {
  function finAllSessions ({ where }) {
    return MailSessionModel.findAll({ where })
  }

  function findAllMails ({ where, page, pageSize }) {
    const offset = (page - 1) * pageSize
    return MailModel.findAndCountAll({
      where,
      limit: pageSize,
      offset: offset
    })
  }

  function updateSession (model) {
    return MailSessionModel.update(model, { where: { id: model.id } })
  }

  function findAllSessionClients ({ where, pageSize }) {
    const { campaniaId, mailSessionId } = where
    const query = `select
      count(*) as count
      from campania_clientes_owner as cco
      INNER JOIN marketing_no_clientes mnc on mnc.id = cco.cliente_id
      left join mail m on m.email =  mnc.correo and m.mail_session_id = :mailSessionId
      where cco.campania_id = :campaniaId and m.id IS NULL `
    const select = ` mnc.id as clientId,
       mnc.documento as document,
       CONCAT(mnc.nombres,' ', mnc.apellidos) as fullName,
       mnc.correo as email,
       mnc.merge_vars as details`
    const limit = pageSize
    return new Promise(async (resolve, reject) => {
      console.log('okojojas')
      const { count } = await CampaignClientOwnerModel.sequelize.query(query, {
        type: QueryTypes.SELECT, plain: true, replacements: { mailSessionId, campaniaId }
      }).catch(console.log)
      let rows = []
      if (count > 0) {
        rows = await CampaignClientOwnerModel.sequelize.query(
          `${query.replace('count(*) as count', select)} LIMIT :limit`, {
            // `${query.replace('count(*) as count', select)} LIMIT :offset, :limit`, {
            type: QueryTypes.SELECT, replacements: { limit, mailSessionId, campaniaId }
          }).catch(reject)
      }
      resolve({ count, rows })
    })
  }

  function createOrUpdateMail (model) {
    return new Promise(async (resolve, reject) => {
      const { id } = model
      let instanceModel = id ? await MailModel.findOne({ where: { id } }) : null
      if (instanceModel) {
        model.fechaActualizacion = new Date().toLocaleString()
        MailModel.update(model, { where: { id } })
          .then(() => resolve({ ...instanceModel, ...model }))
          .catch(reject)
      } else {
        MailModel.create(model).then(result => resolve(result)).catch(reject)
      }
    })
  }

  function bulkCreateOrUpdate (models) {
    return MailModel.bulkCreate(models, {
      updateOnDuplicate: [
        'estado', 'fechaEnvio',
        'mandrillId', 'fechaActualizacion',
        'mandrillResponse', 'opens',
        'opensDetails', 'clicks',
        'clicksDetails', 'inspectorResponse',
        'inspectorAt']
    })
  }

  return {
    finAllSessions,
    findAllMails,
    updateSession,
    bulkCreateOrUpdate,
    findAllSessionClients,
    createOrUpdateMail
  }
}
